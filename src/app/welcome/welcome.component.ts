import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { WelcomeModalComponent } from './welcome-modal/welcome-modal.component';
import { ChatbotService } from '../chatbot.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    private chatbotService: ChatbotService
  ) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.openDialog();
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(WelcomeModalComponent, {
      width: '400px',
      data: { name: 'name', animal: 'animal' }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('!!! The intro dialog was closed. Welcome !!!');
      this.chatbotService.showWelcomeMessage();
    });
  }
}
