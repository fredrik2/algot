import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AuthService } from '../auth.service';
import { ChatbotService } from '../chatbot.service'

@Component({
  selector: 'app-bank-id',
  templateUrl: './bank-id.component.html',
  styleUrls: ['./bank-id.component.scss']
})

export class BankIdComponent {

  code: Number;
  enteredCode: string = '';
  wrongCode: boolean = false;
  buttons: any[];
  buttonColor: string = '#103461';

  constructor(
    public dialogRef: MatDialogRef<BankIdComponent>,
    private authService: AuthService,
    private chatbotService: ChatbotService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.buttons = [
      {text: 1, val: 1, color: this.buttonColor},
      {text: 2, val: 2, color: this.buttonColor},
      {text: 3, val: 3, color: this.buttonColor},
      {text: 4, val: 4, color: this.buttonColor},
      {text: 5, val: 5, color: this.buttonColor},
      {text: 6, val: 6, color: this.buttonColor},
      {text: 7, val: 7, color: this.buttonColor},
      {text: 8, val: 8, color: this.buttonColor},
      {text: 9, val: 9, color: this.buttonColor},
      {text: '', val: null, color: 'white'},
      {text: 0, val: 0, color: this.buttonColor},
      {text: 'Ok', val: 'ok', color: '#F8C523'},
    ]
  }

  ngOnInit() {
    this.code = this.authService.mockCode();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public gridClick(value:string) {
    console.log('Got value',value);
    if (value == 'ok') {
      if (this.enteredCode === this.code.toString()) {
        this.chatbotService.communicate('BankId', 'internal');
        this.dialogRef.close();
      } else {
        this.wrongCode = true;
      }
    } else {
      this.enteredCode = this.enteredCode +''+ value;
      console.log('Code is now:', this.enteredCode);
    }
  }
}
