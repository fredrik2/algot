import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankIdComponent } from './bank-id.component';

describe('BankIdComponent', () => {
  let component: BankIdComponent;
  let fixture: ComponentFixture<BankIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
