import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

import * as AWS from 'aws-sdk';

@Injectable()
export class ChatbotService {

  private lexRuntime;
  private lexUserId;
  private sessionAttributes;

  private isCommunicating: boolean = true;
  private welcomeSent: boolean = false;
  private chatMessages = new Subject<any>();

  constructor(
  ) {
    AWS.config.region = 'eu-west-1'; // AWS Region
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
		// Provide your Pool Id here
			IdentityPoolId: 'eu-west-1:62d754e1-6754-443b-84c1-0c3935cc3300',
		});

		this.lexRuntime = new AWS.LexRuntime();
		this.lexUserId = 'chatbot-demo-' + Date.now();
		this.sessionAttributes = {};
  }

  public listen() {
    return this.chatMessages.asObservable();
  }

  public isLocked() {
    return this.isCommunicating;
  }

  public communicate(chatMsg, type:'send'|'internal'='send') {
    this.isCommunicating = true;
    return new Promise((resolve, reject) => {
      if (chatMsg && chatMsg.trim().length > 0) {
        chatMsg = chatMsg.trim();
        let wisdom = this.aao(chatMsg, 'send');

        if (type != 'internal') this.chatMessages.next({message:chatMsg, type:type});

        // send it to the Lex runtime
        let params = {
          botAlias: 'prod',
          botName: 'NamnBytarBoten',
          inputText: wisdom,
          userId: this.lexUserId,
          sessionAttributes: this.sessionAttributes
        };
        console.log('ChatbotService got: ', params);

        this.lexRuntime.postText(params, (err, data) => {
          if (err) {
            this.digestData({message: 'Ojdå. Något gick lite fel här.'}, 'error');
            this.digestData(err, 'error')
            this.isCommunicating = false;
            reject(err);
          }
          if (data) {
            // capture the sessionAttributes for the next cycle
            this.sessionAttributes = data.sessionAttributes;
            this.digestData(data, 'receive');
            this.isCommunicating = false;
            resolve(data);
          }
        });
      } else {
        this.isCommunicating = false;
        reject('No message at all!');
      }
    });
  }

  private digestData(data:any, type: 'send'|'receive'|'error'|'internal'):any {
    console.log('Digesting Message:',data);
    let message = data.message;
    let chatMessage = {};
    let infoButton = this.infoButton(message);

    if (infoButton) {
      message = infoButton.message;
      chatMessage['infoText'] = infoButton.infoText;
      chatMessage['messageTrail'] = this.aao(infoButton.messageTrail, 'receive');
    }

    chatMessage['message'] = this.aao(message, 'receive');
    chatMessage['type'] = type;

    if (data.hasOwnProperty('responseCard')) {
      chatMessage['responseCards'] = data.responseCard.genericAttachments[0];
    }

    this.chatMessages.next(chatMessage);
  }

  public showWelcomeMessage(id:number = 1) {
    if (!this.welcomeSent) {
      const message = {
        message: 'Hej Gun! Jag heter Algot och är Skatteverkets namnändrarassistent.',
        type: 'receive'
      }
      console.log('Sending welcome message', message);
      this.chatMessages.next(message);

      console.log('Getting welcome from backend for idnr '+id);
      this.communicate('idnr '+id, 'internal').then(r => {
        this.welcomeSent = true;
      }, e => {
        console.log('welcome fialed!', e);
        const errMsg = {
          message: 'Kunde inte koppla upp mot systemet. Tyvärr måste jag be dig att börja om igen.',
          type: 'error'
        }
        this.isCommunicating = true;
        this.chatMessages.next(errMsg);
      });
    }
  }

  public feMessage(message, type: 'send'|'receive'|'error'):void {
    const msg = {message: message, type: type};
    this.chatMessages.next(msg);
  }

  private aao(msg:string, direction:'receive'|'send'):string {
    if (direction == 'receive') {
      msg = msg.replace(/aaaa/g, 'ä');
      msg = msg.replace(/aaa/g, 'å');
      msg = msg.replace(/oooo/g, 'ö');
      msg = msg.replace(/AAAA/g, 'Ä');
      msg = msg.replace(/AAA/g, 'Å');
      msg = msg.replace(/OOOO/g, 'Ö');
    } else if (direction == 'send'){
      msg = msg.replace(/[åÅäÄ]/g, 'a');
      msg = msg.replace(/[öÖ]/g, 'o');
    }
    return msg;
  }

  private infoButton(msg):any{
    let infoText = msg.match(/\[info\]([^}]*)\[\/info\]/);
    if (infoText) {
      msg = msg.replace(/\[info\]([^}]*)\[\/info\]/, '|--|');
      let msgArr = msg.split('|--|');
      return {message: msgArr[0], infoText: infoText[1], messageTrail: msgArr[1]};
    } else {
      return null;
    }
  }
}
