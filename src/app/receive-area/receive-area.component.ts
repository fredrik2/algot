import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { BankIdComponent } from '../bank-id/bank-id.component';
import { ChatbotService } from '../chatbot.service';

@Component({
  selector: 'app-receive-area',
  templateUrl: './receive-area.component.html',
  styleUrls: ['./receive-area.component.css']
})
export class ReceiveAreaComponent implements OnInit {

  public messages: any[] = [];
  private timeout: any;

  // Not needed but from dialog example
  public animal: string = 'Djur';
  public name: string = 'Namn';

  constructor(
    private chatbotService: ChatbotService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.chatbotService.listen().subscribe(message => {
      console.log('Got message: ', message);

      // Kill any timeouts.
      if (this.timeout) clearTimeout(this.timeout);

      // Message Color
      let msgClass = 'receive';
      if (message.type == 'send') msgClass = 'send';
      if (message.type == 'error') msgClass = 'error';

      let person = 'Algot';
      if (message.type == 'send') person = 'Du';
      message.person = person;

      message['class'] = msgClass;

      // Remove buttons from prev message
      let prevMsgIndex = this.messages.length-1;
      if (this.messages.length > 0 && this.messages[prevMsgIndex].hasOwnProperty('responseCards')) {
        console.log('Removing: ',this.messages[prevMsgIndex]['responseCards']);
        this.messages[prevMsgIndex]['hideCards'] = true;
      }

      // Add the message to chat.
      this.messages.push(message);
      window.scrollTo(0,document.body.scrollHeight);

      // If the user is expected to write something focus on the textarea.
      // @NOTE: This fix introduces a calssic race condition
      if (!message.hasOwnProperty('responseCards')) {
        console.log('No buttons. User is expected to write.');
        this.timeout = setTimeout(()=> {
          console.log('typeMsg should now be in focus.');
          document.getElementById('typeMsg').focus();
        }, 250);
      }
    });
  }

  public buttonClick(button) {
    // is this a special button?
    if (button.value == 'BankId') {
      console.log('Got BankId');
      this.openDialog();
    } else {
      console.log('Button clicked', button);
      this.chatbotService.communicate(button.value, 'internal');
      this.chatbotService.feMessage(button.text, 'send');
    }
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(BankIdComponent, {
      width: '250px',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

}
