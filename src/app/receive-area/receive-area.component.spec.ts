import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveAreaComponent } from './receive-area.component';

describe('ReceiveAreaComponent', () => {
  let component: ReceiveAreaComponent;
  let fixture: ComponentFixture<ReceiveAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiveAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
