import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatDividerModule,
  MatGridListModule,
  MatDialogModule,
  MatProgressSpinnerModule,
  MatExpansionModule
} from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatDividerModule,
    MatGridListModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatExpansionModule
  ],
  exports: [
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatIconModule,
    MatInputModule,
    MatDividerModule,
    MatGridListModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatExpansionModule
  ],
})
export class MaterialModule { }
