import { Component, OnInit } from '@angular/core';
import { ChatbotService } from '../chatbot.service';

@Component({
  selector: 'app-type-area',
  templateUrl: './type-area.component.html',
  styleUrls: ['./type-area.component.css']
})
export class TypeAreaComponent implements OnInit {

  public chatMsg: string;
  private chatBox: any;

  constructor(private chatbotService: ChatbotService) { }

  ngOnInit() {
      this.chatBox = document.getElementById('typeMsg');
      this.chatBox.focus();
  }

  public checkEnter(event) {
    if (event.which == 13) {
      console.log('Got enter key');
      this.sendMessage();
    }
  }

  public sendMessage() {
    console.log('Sending off msg: ', this.chatMsg);
    this.chatbotService.communicate(this.chatMsg).then(r => {
      console.log('Chat response:',r);
      this.chatMsg = null;
    }, e => {
      console.error('Chat error!', e);
      this.chatMsg = null;
    });
    this.chatMsg = '...';
  }

  public get chatLocked() {
    return this.chatbotService.isLocked();
  }

}
