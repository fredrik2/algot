import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeAreaComponent } from './type-area.component';

describe('TypeAreaComponent', () => {
  let component: TypeAreaComponent;
  let fixture: ComponentFixture<TypeAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
