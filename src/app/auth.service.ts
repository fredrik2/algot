import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  private code: number;

  constructor() { }

  public mockCode():Number {
    this.code = Math.floor((Math.random() * 1000000) + 1);
    if (this.code < 100000 ) this.code = this.code+100000;
    return this.code;
  }

  public verifyCode(code) {
    if (code == this.code) {
      return true
    } else {
      return false;
    }
  }

}
