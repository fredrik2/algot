import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';

import { AppComponent } from './app.component';
import { TypeAreaComponent } from './type-area/type-area.component';
import { ReceiveAreaComponent } from './receive-area/receive-area.component';
import { BankIdComponent } from './bank-id/bank-id.component';

import { ChatbotService } from './chatbot.service';
import { AuthService } from './auth.service';
import { WelcomeComponent } from './welcome/welcome.component';
import { WelcomeModalComponent } from './welcome/welcome-modal/welcome-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    TypeAreaComponent,
    ReceiveAreaComponent,
    BankIdComponent,
    WelcomeComponent,
    WelcomeModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [
    ChatbotService,
    AuthService
  ],
  entryComponents: [
    BankIdComponent,
    WelcomeModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
